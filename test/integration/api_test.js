const chai = require('chai')
const expect = require('chai').expect
const chaihttp = require('chai-http')
chai.use(chaihttp)
let server
before((done) => {
  server = require('../../index').server.listen(done)
})
after(async () => {
  const deleteUserTesting = require('../../src/modules/repositories/commands/commands/user_delete_command')
  const data = {
    where: {
      phone_no: '089655440395'
    }
  }
  deleteUserTesting.delete(data)
  await server.close()
})
describe('User Profile Setting Testing', () => {
  it('Show API Version', (done) => {
    chai.request(server).get('/api/v1')
      .end(function (nil, res) {
        expect(res).to.have.status(200)
        done()
      })
  })
})
describe('Login dan Register', function () {
  it('Register User Success', function (done) {
    chai.request(server).post('/api/v1/register')
      .send({
        first_name: 'akun testing',
        last_name: 'akun testing',
        phone_no: '089655440395'
      })
      .end(function (nil, res) {
        expect(res).to.have.status(200)
        done()
      })
  })
  it('Register Otp Success', function (done) {
    chai.request(server).post('/api/v1/register/otp')
      .send({
        first_name: 'akun testing',
        last_name: 'akun testing',
        phone_no: '089655440395',
        role: 'testing',
        otp: '999999'
      })
      .end(function (nil, res) {
        expect(res).to.have.status(200)
        done()
      })
  })
  it('Register Fail', function (done) {
    chai.request(server).post('/api/v1/register')
      .send({
        first_name: 'akun testing',
        last_name: 'akun testing',
        phone_no: '089655440395',
        role: 'testing'
      })
      .end(function (nil, res) {
        expect(res).to.have.status(401)
        done()
      })
  })
  it('Register Otp Fail', function (done) {
    chai.request(server).post('/api/v1/register/otp')
      .send({
        first_name: 'akun testing',
        last_name: 'akun testing',
        phone_no: '089655440395',
        role: 'testing',
        otp: 123456
      })
      .end(function (nil, res) {
        expect(res).to.have.status(401)
        done()
      })
  })
  it('Login User Success', function (done) {
    chai.request(server).post('/api/v1/login')
      .send({
        phone_no: '089655440395',
        password: '089655440395'
      })
      .end(function (nil, res) {
        expect(res).to.have.status(200)
        done()
      })
  })
  it('Login Fail', function (done) {
    chai.request(server).post('/api/v1/login')
      .send({
        phone_no: '089123456781',
        password: 'password'
      })
      .end(function (nil, res) {
        expect(res).to.have.status(404)
        done()
      })
  })
})
describe('User Profile Testing', function () {
  let user = {}
  it('Read User Success', function (done) {
    chai.request(server).get('/api/v1/user/profile/089655440395')
      .end(function (nil, res) {
        user = res.body
        expect(res).to.have.status(200)
        done()
      })
  })
  it('Read User Fail Not Found', function (done) {
    chai.request(server).get('/api/v1/user/profile/08912345678')
      .end(function (nil, res) {
        expect(res).to.have.status(404)
        done()
      })
  })
  it('Change User Password Success', function (done) {
    chai.request(server).put('/api/v1/user/changepass')
      .send({
        phone_no: '089655440395',
        password: '089655440395',
        newpassword: '123455555'
      })
      .end(function (nil, res) {
        expect(res).to.have.status(200)
        done()
      })
  })
  it('Change User Password Fail Not Found', function (done) {
    chai.request(server).put('/api/v1/user/changepass')
      .send({
        phone_no: '08912345678',
        password: '123455555',
        newpassword: '123455555'
      })
      .end(function (nil, res) {
        expect(res).to.have.status(404)
        done()
      })
  })
  it('Reset User Password Success', function (done) {
    chai.request(server).get('/api/v1/user/resetpass/089655440395')
      .end(function (nil, res) {
        expect(res).to.have.status(200)
        done()
      })
  })
  it('Reset User Password Fail Not Found', function (done) {
    chai.request(server).get('/api/v1/user/resetpass/08912345678')
      .end(function (nil, res) {
        expect(res).to.have.status(404)
        done()
      })
  })
  it('Update User Profile Success', function (done) {
    chai.request(server).put('/api/v1/user/profile/' + user.data.phone_no)
      .send({
        first_name: 'akun edit',
        last_name: 'akun edit',
        phone_no: '089655440395',
        password: 'password',
        address_street: 'akun edit',
        address_postal: '012345',
        address_province: 'akun edit',
        address_city: 'akun edit',
        address_district: 'akun edit',
        address_village: 'akun edit',
        ktp_no: 'akun edit',
        email: 'akun@edit.com',
        photo: 'akun edit',
        role: 'testing'
      })
      .end(function (nil, res) {
        expect(res).to.have.status(200)
        user = res.body
        done()
      })
  })
  it('Update User Profile Fail Notfound', function (done) {
    chai.request(server).put('/api/v1/user/profile/08123918237')
      .send({
        first_name: 'akun edit',
        last_name: 'akun edit',
        phone_no: '089655440395',
        password: 'password',
        address_street: 'akun edit',
        address_postal: '012345',
        address_province: 'akun edit',
        address_city: 'akun edit',
        address_district: 'akun edit',
        address_village: 'akun edit',
        ktp_no: 'akun edit',
        email: 'akun@edit.com',
        photo: 'akun edit',
        role: 'testing'
      })
      .end(function (nil, res) {
        expect(res).to.have.status(404)
        done()
      })
  })
})
