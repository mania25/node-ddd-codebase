
# User Service Backend

# Requirement

- Node 8+
- npm 5.++
- KoaJS 2.++

# Design Pattern

- DDD (Domain Driven Development)

# Pattern Flow (In src folder)

`app/routes` -> `modules/handlers/api` -> `modules/handlers/repositories/commands/handlers` -> `modules/handlers/repositories/commands/domains` -> `modules/handlers/repositories/commands/commands`

# Directory Explanation

- `test`: folder where all unit test are stored.
- `config`: folder where all application configuration are stored.
- `src/app`: folder where all server and routes initialization are stored.
- `src/helpers`: folder where all helper function are stored.
- `src/infra`: still unknown folder.
- `src/middlewares`: folder where all middleware-related function are stored.
- `src/modules/handlers/api`: folder where all api handlers are stored.
- `src/modules/repositores/commands/commands`: folder where all command (bottom layer that will interact with database) files are stored.
- `src/modules/repositores/commands/domains`: folder where all command domain files are stored.
- `src/modules/repositores/commands/handlers`: folder where all command handler files are stored.
- `src/modules/repositores/commands/models`: folder where all command model files are stored.