'use strict'

require('dotenv').config({
  path: `./config/${process.env.MODE}/.env`
})
const pino = require('pino')()
const SequelizeConnection = require('./src/helpers/databases/sql/db')
const MongooseConnection = require('./src/helpers/databases/mongodb/db')
const AppServer = require('./src/app/server')
const appServer = new AppServer()
const port = process.env.PORT || 1337

appServer.server.listen(port, async () => {
  const connection = new SequelizeConnection()
  const mongoConnection = new MongooseConnection()

  await connection.createConnection()
  await mongoConnection.createConnection()

  if (connection.getConnectionStatus()) {
    pino.info(`Connection to ${process.env.DB_NAME} database has been established successfully.`)
  } else {
    pino.error(`Connection to ${process.env.DB_NAME} database has been failed.`)
  }

  if (mongoConnection.getConnectionStatus()) {
    pino.info(`Connection to Mongo database has been established successfully.`)
  } else {
    pino.error(`Connection to Mongo database has been failed.`)
  }

  pino.info(`Server is running on port ${port}`)
})

module.exports = appServer
