const validate = require('../../../helpers/utils/validate')

const isValidEmail = async (email) => {
  const isValid = await validate.validateEmail(email)
  return isValid
}
module.exports = isValidEmail
