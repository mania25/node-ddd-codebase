const validate = require('../../../helpers/utils/validate')

const isValidPhoneNumber = async (payload) => {
  let constraints = {}
  let values = {}

  constraints[payload.phone_no] = {presence: true, length: {minimum: 9}}
  values[payload.phone_no] = payload.phone_no

  const isValid = await validate.validateConstraints(values, constraints)

  return isValid
}

module.exports = isValidPhoneNumber
