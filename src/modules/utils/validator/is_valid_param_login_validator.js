const validate = require('../../../helpers/utils/validate')

const isValidParamLogin = async (payload) => {
  let constraints = {}
  let values = {}

  constraints[payload.phone_no] = {presence: true, length: {minimum: 9}}
  constraints[payload.password] = {presence: true, length: {minimum: 4}}
  values[payload.phone_no] = payload.phone_no
  values[payload.password] = payload.password

  const isValid = await validate.validateConstraints(values, constraints)

  return isValid
}

module.exports = isValidParamLogin
