
const isValidParamLoginValidator = require('./is_valid_param_login_validator')
const isValidPhoneNumber = require('./is_valid_phone_number')
const isValidEmail = require('./is_valid_email')
const isValidOtp = require('./is_valid_otp')

module.exports = {
  isValidParamLoginValidator: isValidParamLoginValidator,
  isValidPhoneNumber: isValidPhoneNumber,
  isValidEmail: isValidEmail,
  isValidOtp: isValidOtp
}
