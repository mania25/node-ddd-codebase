'use strict'

const showAPIVersion = (ctx) => {
  ctx.res.ok({
    version: '1.0.0'
  })
}

module.exports = showAPIVersion
