const showAPIVersion = require('./show_api_version')

module.exports = {
  showAPIVersion: showAPIVersion
}
