const middlewareResponse = require('./response')
const middlewareValidation = require('./validation')

module.exports = {
  middlewareResponse: middlewareResponse,
  middlewareValidation: middlewareValidation
}
