'use strict'

const Sequelize = require('sequelize')
const pino = require('pino')()
const wrapper = require('../../utils/wrapper')

class DB {
  constructor () {
    this.statePool = false
  }

  getConnectionStatus () {
    return this.statePool
  }

  getConnection () {
    return this.pool
  }

  setConnection () {
    this.statePool = true
  }

  done (connection) {
    connection.close()
    this.statePool = false
  }

  async createConnection () {
    try {
      this.pool = new Sequelize(
        process.env.DB_NAME,
        process.env.DB_USER,
        process.env.DB_PASSWORD,
        {
          host: process.env.DB_HOST,
          port: process.env.DB_PORT,
          dialect: process.env.DB_ADAPTER,
          operatorsAliases: false,
          logging: process.env.MODE === 'development'
        }
      )
      await this.pool.authenticate()
      await this.pool.sync({
        force: process.env.MODE === 'development'
      })
      this.setConnection()
    } catch (err) {
      pino.error(`Failed to connect Postgresql Database: ${err.message}`)
      wrapper.error(err, `Failed to connect Postgresql Database: ${err.message}`, 500)
    }
  }
}

module.exports = DB
