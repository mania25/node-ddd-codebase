'use strict'

const mongoose = require('mongoose')
const pino = require('pino')()
const wrapper = require('../../utils/wrapper')

class DB {
  constructor () {
    this.statePool = false
  }

  getConnectionStatus () {
    return this.statePool
  }

  getConnection () {
    return this.pool
  }

  setConnection () {
    this.statePool = true
  }

  done (connection) {
    connection.close()
    this.statePool = false
  }

  async createConnection () {
    try {
      const options = {
        reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
        reconnectInterval: 500, // Reconnect every 500ms
        poolSize: 10, // Maintain up to 10 socket connections
        // If not connected, return errors immediately rather than waiting for reconnect
        bufferMaxEntries: 0
      }
      this.pool = await mongoose.connect(process.env.MONGO_DATABASE_URL, options)
      this.setConnection()
    } catch (err) {
      pino.error(`Failed to connect Postgresql Database: ${err.message}`)
      wrapper.error(err, `Failed to connect Mongo Database: ${err.message}`, 500)
    }
  }
}

module.exports = DB
