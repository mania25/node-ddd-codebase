'use strict'

const AWS = require('aws-sdk')
const pino = require('pino')()

class SQS {
  constructor (config) {
    this.config = config
  }

  init () {
    AWS.config.update({
      'region': 'ap-southeast-1'
    })
    this.sqs = new AWS.SQS()
  }

  receiveQueue (cb) {
    const sqs = this.sqs
    const config = this.config
    sqs.receiveMessage(config, (err, data) => {
      if (err) {
        cb(err, null, null)
      } else if (data.Messages) {
        const message = data.Messages[0]
        const receipt = message.ReceiptHandle
        const body = JSON.parse(message.Body)
        cb(null, body.Message, receipt)
      } else {
        cb(null, ``, null)
      }
    })
  }

  removeQueue (receiptHandle) {
    const sqs = this.sqs
    const config = this.config
    sqs.deleteMessage({
      QueueUrl: config.QueueUrl,
      ReceiptHandle: receiptHandle
    }, (err, data) => {
      (err) ? pino.error(`err, ${err}`) : pino.info(`Message Deleted`)
    })
  };
}

module.exports = SQS
