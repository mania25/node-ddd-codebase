const AWS = require('aws-sdk')

const s3 = new AWS.S3({
  accessKeyId: process.env.MINIO_ACCESS_KEY,
  secretAccessKey: process.env.MINIO_SECRET_KEY,
  endpoint: process.env.MINIO_BASE_URL,
  s3ForcePathStyle: true, // needed with minio?
  signatureVersion: 'v4'
})

// Bucket names must be unique across all S3 users
let res = {
  status: false,
  data: null
}
function bucketCreate (bucketName) {
  s3.createBucket({Bucket: bucketName}, function (err, data) {
    if (err) {
      res.data = err
      return res
    } else {
      res.status = true
      return res
    }
  })
}

module.exports = {
  bucketCreate: bucketCreate
}
