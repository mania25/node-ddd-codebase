const AWS = require('aws-sdk')
const fs = require('fs')

const s3 = new AWS.S3({
  accessKeyId: process.env.MINIO_ACCESS_KEY,
  secretAccessKey: process.env.MINIO_SECRET_KEY,
  endpoint: process.env.MINIO_BASE_URL,
  s3ForcePathStyle: true, // needed with minio?
  signatureVersion: 'v4'
})

let res = {
  status: false,
  data: null
}
// getObject operation.
function objectUrl (bucketName, keyName) {
  return new Promise((resolve, reject) => {
    const urlParams = {
      Bucket: bucketName,
      Key: keyName
    }
    s3.getSignedUrl('getObject', urlParams, function (err, url) {
      if (err) {
        console.log('Cannot get image url : ' + err.message, url)
        res.data = err
        reject(res)
      } else {
        res.status = true
        res.data = url
        resolve(res)
      }
    })
  })
}
function objectDownload (bucketName, keyName, saveTo) {
  return new Promise((resolve, reject) => {
    const file = fs.createWriteStream(saveTo)
    var params = {Bucket: bucketName, Key: keyName}
    s3.getObject(params)
      .on('httpData', function (chunk) {
        file.write(chunk)
      })
      .on('httpDone', function () {
        file.end()
      })
      .send()
    res.status = true
    resolve(res)
  })
}

function objectUpload (bucketName, keyName, filePath) {
  return new Promise((resolve, reject) => {
    fs.readFile(filePath, (err, data) => {
      if (err) {
        console.log(err.message)
        res.data = err
        reject(res)
      } else {
        var params = {Bucket: bucketName, Key: keyName, Body: data}
        s3.putObject(params, function (err, data) {
          if (err) {
            console.log('Error uploading data : ' + err.message, data)
            res.data = err
            reject(res)
          } else {
            console.log('Successfully uploaded data')
            res.status = true
            res.data = data
            resolve(res)
          }
        })
      }
    })
  })
}

module.exports = {
  objectUrl: objectUrl,
  objectDownload: objectDownload,
  objectUpload: objectUpload
}
