const bucket = require('./bucket_helper')
const object = require('./object_helper')

module.exports = {
  bucket: bucket,
  object: object
}
