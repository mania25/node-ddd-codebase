'use strict'

const Raven = require('raven')

const sendError = async errorMessage => {
  Raven.config(process.env.DSN_SENTRY_URL).install()
  Raven.captureException(errorMessage)
}

module.exports = {
  sendError: sendError
}
