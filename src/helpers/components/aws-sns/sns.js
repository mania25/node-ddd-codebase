'use strict'

const AWS = require('aws-sdk')

class SNS {
  constructor (topicARN) {
    this.topicARN = topicARN
  }

  init () {
    AWS.config.update({
      'region': 'ap-southeast-1'
    })
    this.sns = new AWS.SNS()
  }

  publishTopic (payload, topicARN = this.topicARN, eventType = null) {
    const sns = this.sns
    if (!eventType) {
      eventType = payload.eventType
    }
    return new Promise((resolve, reject) => {
      sns.publish({
        TopicArn: topicARN,
        Message: JSON.stringify(payload),
        Subject: eventType
      }, (err, data) => {
        if (err) {
          return reject(err)
        } else {
          return resolve(data.MessageId)
        }
      })
    })
  }
}

module.exports = SNS
