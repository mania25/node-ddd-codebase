const bcrypt = require('bcrypt')

const hashPassword = (saltRound, plainTextPassword) => {
  const salt = bcrypt.genSaltSync(saltRound)

  return bcrypt.hashSync(plainTextPassword, salt)
}

const verifyHashPassword = (plainTextPassword, hashedPassword) => {
  return bcrypt.compareSync(plainTextPassword, hashedPassword)
}

module.exports = {
  hashPassword: hashPassword,
  verifyHashPassword: verifyHashPassword
}
