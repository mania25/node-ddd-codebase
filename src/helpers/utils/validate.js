const validate = require('validate.js')
const validator = require('validator')
const wrapper = require('../../helpers/utils/wrapper')

const validateConstraints = async (values, constraints) => {
  if (validate(values, constraints)) {
    return wrapper.error('Bad Request', validate(values, constraints), 400)
  } else {
    return wrapper.data(true, 'Param is valid', 200)
  }
}

const validateEmail = async (values) => {
  if (validator.isEmail(values)) {
    return wrapper.data(true, 'Param is valid', 200)
  } else {
    return wrapper.error('Bad Request', 'Param invalid', 400)
  }
}
module.exports = {
  validateConstraints: validateConstraints,
  validateEmail: validateEmail
}
