'use strict'

const Koa = require('koa')
const cors = require('kcors')
const koaBody = require('koa-body')
const logger = require('koa-logger')
const json = require('koa-json')
const bouncer = require('koa-bouncer')
const onerror = require('koa-onerror')
const pino = require('pino')()

const middleware = require('../middlewares')

let AppServer = function () {
  this.server = new Koa()

  onerror(this.server)

  this.server.use(
    koaBody({ multipart: true, json: true, urlencoded: false, text: false })
  )
  this.server.use(json())
  this.server.use(logger())
  this.server.use(cors())
  this.server.use(bouncer.middleware())
  this.server.use(
    middleware.middlewareValidation.handleBouncerValidationError()
  )
  this.server.use(
    middleware.middlewareResponse({ contentType: `application/json` })
  )

  const routes = require('./routes')(this.server)
  this.server.use(routes.routes(), routes.allowedMethods())

  // logger
  this.server.use(async (ctx, next) => {
    const start = new Date()
    await next()
    const ms = new Date() - start
    pino.info(`${ctx.method} ${ctx.url} - ${ms}ms`)
  })

  // error-handling
  this.server.on('error', (err, ctx) => {
    pino.error('server error', err, ctx)
  })
}

module.exports = AppServer
