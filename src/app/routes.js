'use strict'

const Router = require('koa-router')
const APIHandler = require('../modules/handlers/api')

module.exports = app => {
  const router = new Router({
    prefix: '/api/v1'
  })

  router.get('/', APIHandler.showAPIVersion)

  return router
}
